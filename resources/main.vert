#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 3) in vec2 inTexCoords;
layout (location = 4) in vec3 inNormal;

out vec2 passTexCoords;
out vec3 worldPosition;
out vec3 worldNormal;

uniform mat4 trans;
uniform mat4 model;

void main() {
    gl_Position = trans * vec4(inPosition, 1);
    worldPosition = vec3(model * vec4(inPosition, 1));
    // This normal matrix should be calculated once on the CPU
    // https://learnopengl.com/Lighting/Basic-Lighting
    worldNormal = mat3(transpose(inverse(model))) * inNormal;
    passTexCoords = inTexCoords;
}
