#version 330 core
out vec3 outColor;

uniform vec3 color;

void main() {
    outColor = color;
}
