#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 3) in vec2 inTexCoords;

out vec2 passTexCoords;
out vec2 passPosition;

void main() {
    gl_Position = vec4(inPosition.xy, 0, 1);
    passPosition = inPosition.xy;
    passTexCoords = inTexCoords;
}
