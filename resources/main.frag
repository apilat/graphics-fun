#version 330 core
in vec2 passTexCoords;
in vec3 worldPosition;
in vec3 worldNormal;

out vec3 outColor;

uniform sampler2D textureSampler;
uniform bool light;
uniform vec3 lightSource;
uniform bool useTexture;
#define PI 3.1415926538

void main() {
    vec3 textureColor;
    if (useTexture) {
        textureColor = texture(textureSampler, passTexCoords).rgb;
    } else {
        textureColor = vec3(
            abs(passTexCoords.x - 0.5) * 2,
            abs(passTexCoords.y - 0.5) * 2,
            0
        );
    }

    float colorModifier;
    if (light) {
        colorModifier = 0.2;
        vec3 lightDirection = lightSource - worldPosition;
        colorModifier += 0.8 * max(0, dot(normalize(worldNormal), normalize(lightDirection)));
    } else {
        colorModifier = 1.0;
    }

    outColor = colorModifier * textureColor;
}
