#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inColor;
out vec3 passColor;

uniform mat4 trans;

void main() {
    gl_Position = trans * vec4(position, 1);
    passColor = inColor;
}
