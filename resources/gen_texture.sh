#!/bin/bash
[ $# -eq 1 ] || {
    echo >&2 "Usage: $0 file.png"
    exit 1
}

O="$1"
W=$(convert "$O" -format "%[w]" info:)
H=$(convert "$O" -format "%[h]" info:)
N=${1%.png}.dat

python3<<EOF
import struct
with open("$N", "wb") as f:
    f.write(struct.pack("<II", $W, $H))
EOF

stream -storage-type char "$O" - >> "$N"
