#version 330 core
in vec2 passTexCoords;
in vec2 passPosition;

out vec3 color;

uniform sampler2D screenTexture;
uniform bool flipx;
uniform bool flipy;
uniform bool invert;
uniform bool wave;
uniform float time;
uniform bool scope;
uniform bool chromatic_aberration;

#define PI 3.1415926538

void main() {
    // This can be further optimized if it turns out to be a performance problem

    vec2 texCoords = passTexCoords;
    if (flipx) {
        texCoords.x = -texCoords.x;
    }
    if (flipy) {
        texCoords.y = -texCoords.y;
    }

    if (wave) {
        texCoords.x += 0.02 * sin(time * 2*PI + texCoords.y * PI);
    }

    color = texture(screenTexture, texCoords).rgb;

    if (chromatic_aberration) {
        color = vec3(texture(screenTexture, texCoords - vec2(0.005, 0)).r,
                     texture(screenTexture, texCoords).g,
                     texture(screenTexture, texCoords + vec2(0.005, 0)).b);
    }

    if (invert) {
        color = vec3(1) - color;
    }

    if (scope) {
        if (min(abs(passPosition.x), abs(passPosition.y)) < 0.002) {
            color = vec3(1, 0, 0);
        }

        // dot(x, x) = magnitude squared
        float squaredDistanceToCenter = dot(passPosition, passPosition);
        color *= exp(-6.0 * squaredDistanceToCenter);
    }
}
