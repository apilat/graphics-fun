#version 330 core
in vec3 passColor;

out vec3 outColor;

void main() {
    outColor = passColor;
}
