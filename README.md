# graphics-fun

This project is a set of my experiments with computer graphics utilizing OpenGL.
It consists of a desktop Linux application which renders some simple resources and shaders,
and is controlled by a rudimentary HTTP API
(I concluded this was easier than trying to implement a UI in pure OpenGL).

## Usage

Before the first run, we need to convert images into a format this app can understand.
Run `resources/gen_texture.sh file.png` on the files `resources/cube/cube.png`,
`resources/square/square.png`, and `resources/pyramid/pyramid.png`.

CMake is used as the build system, so you can run the following commands to compile the app
```
cd build
cmake ..
make
```

The main executable uses relative paths so has to be ran from the root of the git repository using `build/main`.
This will also start the control server listening on `localhost:4444`.
