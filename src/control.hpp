#pragma once

#include <thread>
#include <optional>
#include <mutex>
#include <unordered_map>
#include <memory>
#include <string>

namespace control {
    struct Event {
        bool updated;
        std::string data;
    };

    class EventQueue {
        std::mutex mutex;
        std::unordered_map<std::string,Event> events;
    public:
        std::optional<std::string> get(std::string const& source);
        std::optional<std::string> get_stale(std::string const& source);
        void push(std::string const& source, std::string data);
        void updated(std::string const& source);
    };

    std::thread startControlServer(std::shared_ptr<EventQueue> queue);
}
