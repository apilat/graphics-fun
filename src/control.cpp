#include "control.hpp"

#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
#include <sys/socket.h>
#include <netinet/in.h>

namespace control {
    std::optional<std::string> EventQueue::get(std::string const& source) {
        std::optional<std::string> ret = {};
        Event *evt;

        mutex.lock();
        auto it = events.find(source);
        if (it == events.end())
            goto ret;

        evt = &it->second;
        if (!evt->updated)
            goto ret;

        evt->updated = false;
        ret = evt->data;

ret:
        mutex.unlock();
        return ret;
    }

    std::optional<std::string> EventQueue::get_stale(std::string const& source) {
        std::optional<std::string> ret = {};

        mutex.lock();
        auto it = events.find(source);
        if (it == events.end()) {
            goto ret;
        }

        ret = it->second.data;

ret:
        mutex.unlock();
        return ret;
    }

    void EventQueue::push(std::string const& source, std::string data) {
        mutex.lock();
        events[source] = {true, data};
        mutex.unlock();
    }

    void EventQueue::updated(std::string const& source) {
        mutex.lock();
        events[source].updated = true;
        mutex.unlock();
    }

    static void _startServer(std::shared_ptr<EventQueue> queue);
    static void _handleConnection(int peer, std::shared_ptr<EventQueue> queue);
    static void _doResponse(const char* method, const char* location, char* req_body, size_t req_body_len,
            char* resp_body, size_t *resp_body_len, std::shared_ptr<EventQueue> queue);

    std::thread startControlServer(std::shared_ptr<EventQueue> queue) {
        return std::thread(_startServer, queue);
    }

    static void _handleConnection(int peer, std::shared_ptr<EventQueue> queue) {
        char method[8] = {0};
        char location[128] = {0};
        char version[16] = {0};
        char req_body[16384] = {0};
        size_t content_length = 0;

        {
            char buf[17408] = {0};
            ssize_t bytes_read;

            int parse_stage = 1, stage_progress = 0;
            if ((bytes_read = recv(peer, buf, sizeof(buf)-1, 0)) < 0) {
                std::cerr << "Recv failed from fd " << peer << std::endl;
                goto end;
            }

            ssize_t i;
            for (i = 0; i < bytes_read; i++) {
                char c = buf[i];
                switch (parse_stage) {
                    case 1:
                        if (c == ' ') {
                            parse_stage = 2;
                            stage_progress = 0;
                        } else if (stage_progress == sizeof(method)-1) {
                            std::cerr << "Method too long" << std::endl;
                            goto end;
                        } else {
                            method[stage_progress++] = c;
                        }
                        break;

                    case 2:
                        if (c == ' ') {
                            parse_stage = 3;
                            stage_progress = 0;
                        } else if (stage_progress == sizeof(location)-1) {
                            std::cerr << "Location too long" << std::endl;
                            goto end;
                        } else {
                            location[stage_progress++] = c;
                        }
                        break;

                    case 3:
                        if (c == 0xd) {
                            if (i+1 < bytes_read && buf[++i] == 0xa) {
                                parse_stage = 4;
                                stage_progress = 0;
                            } else {
                                std::cerr << "Malformed server version" << std::endl;
                                goto end;
                            }
                        } else if (stage_progress == sizeof(version)-1) {
                            std::cerr << "Version too long" << std::endl;
                            goto end;
                        } else {
                            version[stage_progress++] = c;
                        }
                        break;

                    case 4:
                        {
                            char header_name[128] = {0};
                            size_t header_name_len = 0;
                            char header_value[256] = {0};
                            size_t header_value_len = 0;
                            bool parsing_name = true;

                            for (; i < bytes_read; i++) {
                                char c = buf[i];
                                if (c == 0xd) {
                                    if (i+1 < bytes_read && buf[++i] == 0xa) {
                                        if (parsing_name) {
                                            parse_stage = 5;
                                        } else {
                                            parse_stage = 4;
                                        }
                                        stage_progress = 0;
                                        break;
                                    } else {
                                        std::cerr << "Malformed server version" << std::endl;
                                        goto end;
                                    }
                                }

                                if (parsing_name) {
                                    if (c == ':') {
                                        if (i+1 < bytes_read && buf[++i] == ' ') {
                                            parsing_name = false;
                                        } else {
                                            std::cerr << "Malformed header " << header_name << std::endl;
                                            goto end;
                                        }
                                    } else if (header_name_len == sizeof(header_name)-1) {
                                        std::cerr << "Header name too long" << std::endl;
                                        goto end;
                                    } else {
                                        header_name[header_name_len++] = c;
                                    }
                                } else {
                                    if (header_value_len == sizeof(header_value)-1) {
                                        std::cerr << "Header value too long" << std::endl;
                                        goto end;
                                    } else {
                                        header_value[header_value_len++] = c;
                                    }
                                }
                            }

                            if (strcasecmp(header_name, "content-length") == 0) {
                                char* endptr = header_value;
                                content_length = strtol(header_value, &endptr, 10);
                                if (*endptr != '\0') {
                                    std::cerr << "Content length is not an integer" << std::endl;
                                    goto end;
                                }
                            }
                        }
                        break;

                    case 5:
                        if (i + content_length == (size_t)bytes_read) {
                            memcpy(req_body, buf + i, content_length);
                            parse_stage = 6;
                            stage_progress = 0;
                        } else {
                            std::cerr << "Content length is not accurate" << std::endl;
                            goto end;
                        }
                        break;

                    case 6:
                        goto parse_success;
                };
            }
        }
parse_success:
        if (strcmp(version, "HTTP/1.1") != 0) {
            std::cerr << "Unsupported server version " << version << std::endl;
            goto end;
        }

        {
            char resp_body[16384] = {0};
            size_t resp_body_len = sizeof(resp_body);
            _doResponse(method, location, req_body, content_length, resp_body, &resp_body_len, queue);

            char response[17408] = {0};
            size_t response_len = snprintf(response, sizeof(response), "HTTP/1.1 200 OK\r\n"
                    "content-type: text/html\r\n"
                    "content-length: %zu\r\n"
                    "\r\n" "%s", resp_body_len, resp_body);

            size_t total_sent = 0;
            while (total_sent < response_len) {
                ssize_t packet_sent;
                if ((packet_sent = send(peer, response, response_len, 0)) == -1) {
                    std::cerr << "Send failed to fd " << peer << std::endl;
                    goto end;
                }
                total_sent += packet_sent;
            }
        }

end:
        shutdown(peer, SHUT_RDWR);
    }

    static void _startServer(std::shared_ptr<EventQueue> queue) {
#define fail(msg) do { perror(msg); return; } while(0)
        int sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock == -1)
            fail("socket creation failed");

        struct sockaddr_in listen_addr;
        memset(&listen_addr, 0, sizeof(listen_addr));
        listen_addr.sin_family = AF_INET;
        listen_addr.sin_addr.s_addr = INADDR_ANY;
        listen_addr.sin_port = htons(4444);

        int sockopt = 1;
        if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1)
            fail("setsockopt failed");

        if (bind(sock, (struct sockaddr*) &listen_addr, sizeof(listen_addr)) == -1)
            fail("bind failed");

        if (listen(sock, 3) == -1)
            fail("listen failed");

        std::vector<std::thread> connections;
        while (1) {
            struct sockaddr_in peer_addr;
            socklen_t peer_addr_len = sizeof(peer_addr);
            memcpy(&peer_addr, &listen_addr, sizeof(peer_addr));
            int peer_sock = accept(sock, (struct sockaddr*) &peer_addr, &peer_addr_len);
            if (peer_sock == -1)
                fail("accept failed");

            connections.push_back(std::thread(_handleConnection, peer_sock, queue));
        }

        for (auto &conn : connections)
            conn.join();
    }
#undef fail

    static void _doResponse(const char* method, const char* location, char* req_body, size_t req_body_len,
            char* resp_body, size_t *resp_body_len, std::shared_ptr<EventQueue> queue) {
        (void) location;
        (void) req_body_len;
        if (strcmp(method, "GET") == 0) {
            FILE* f = fopen("resources/index.html", "r");
            if (f == NULL) {
                std::cerr << "Couldn't open index file" << std::endl;
                return;
            }
            *resp_body_len = fread(resp_body, 1, *resp_body_len, f);
            fclose(f);

        } else if (strcmp(method, "POST") == 0) {
            *resp_body = '\0';
            *resp_body_len = 0;

            char* pair;
            while ((pair = strsep(&req_body, "&")) != NULL) {
                char* tmp = strsep(&pair, "=");
                if (tmp == NULL || pair == NULL) {
                    std::cerr << "Invalid argument format" << std::endl;
                    return;
                }
                std::string source(tmp), target(pair);
                queue->push(source, target);
            }
        }
    }
}
