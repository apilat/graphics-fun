#pragma once
#include "../engine/texture.hpp"

#include <map>
#include <memory>

namespace engine {
    class Object;
}

namespace objects {
    enum Type {
        Cube,
        Square,
        Pyramid,
        Torus,
    };

    union Template {
        Type ty;

        struct Torus {
            Type ty;
            unsigned int fragments;
        } torus;
    };

    class Set {
        struct cmpType {
            bool operator()(const Template& a, const Template& b) const {
                if (a.ty != b.ty) {
                    return a.ty < b.ty;
                }

                switch (a.ty) {
                    case Cube:
                    case Square:
                    case Pyramid:
                        return 0;
                    case Torus:
                        return a.torus.fragments < b.torus.fragments;
                }
            };
        };

        std::map< Template, std::shared_ptr<engine::Object>, cmpType > objects;
    public:
        std::shared_ptr<engine::Object> get(Template);
    };
}
