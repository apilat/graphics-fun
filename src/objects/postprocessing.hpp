#pragma once
#include <map>

#include "../engine/shader.hpp"

namespace objects {
    enum PostEffects {
        FlipX,
        FlipY,
        Invert,
        Wave,
        Scope,
        ChromaticAberration,
    };

    class Post {
        std::unique_ptr<engine::Program> shader = std::make_unique<engine::Program>("resources/post");
        std::map< PostEffects, bool > enabledMap;

    public:
        void setEnabled(PostEffects, bool enabled);
        bool isEnabled(PostEffects);

        void bindShader();
        void unbindShader();
    };
}
