#include "objectset.hpp"

#include "../engine/object.hpp"
#include "../engine/mesh.hpp"
#include "../engine/texture.hpp"

#include <stdexcept>
#include <functional>
#include <cmath>

namespace objects {
    // The input functions should parametrize the surface as a function of two variables t, s in [0, 1).
    // The first one should return point positions, and the second surface normals.
    static std::unique_ptr<engine::Mesh> generateMesh2DFromFunction(
            std::function<glm::vec3(float, float)> genPos,
            std::function<glm::vec3(float, float)> genNorm,
            unsigned int frag, bool wrapAround) {
        std::vector<GLfloat> positions, textures, normals;
        std::vector<GLuint> indices;

        auto extent = wrapAround ? frag + 1 : frag;
        for (unsigned int i = 0; i < extent; i++) {
            for (unsigned int j = 0; j < extent; j++) {
                float t = (float) i / frag, s = (float) j / frag;
                auto pos = genPos(t, s);
                positions.push_back(pos.x);
                positions.push_back(pos.y);
                positions.push_back(pos.z);
                textures.push_back(t);
                textures.push_back(s);
                auto norm = genNorm(t, s);
                normals.push_back(norm.x);
                normals.push_back(norm.y);
                normals.push_back(norm.z);
            }
        }

        // We could remove the modulo operations by factoring out the wrapAround
        // code which works on the edges.
        for (unsigned int i = 0; i < extent - 1; i++) {
            for (unsigned int j = 0; j < extent - 1; j++) {
                auto i1 = (i + 1) % extent;
                auto j1 = (j + 1) % extent;
                indices.push_back(i * extent + j);
                indices.push_back(i * extent + j1);
                indices.push_back(i1 * extent + j);

                indices.push_back(i * extent + j1);
                indices.push_back(i1 * extent + j1);
                indices.push_back(i1 * extent + j);
            }
        }

        auto mesh = std::make_unique<engine::Mesh>(positions, indices);
        mesh->getTextureCoords() = textures;
        mesh->getNormals() = normals;
        mesh->syncBuffers();
        return mesh;
    }

    std::shared_ptr<engine::Object> Set::get(Template t) {
        auto obj = objects.find(t);
        if (obj != objects.end()) {
            return obj->second;
        }

        std::shared_ptr<engine::Object> ptr;
        switch (t.ty) {
        case Cube:
            ptr = std::make_shared<engine::Object>("resources/cube/cube");
            break;
        case Square:
            ptr = std::make_shared<engine::Object>("resources/square/square");
            break;
        case Pyramid:
            ptr = std::make_shared<engine::Object>("resources/pyramid/pyramid");
            break;
        case Torus:
            ptr = std::make_shared<engine::Object>(generateMesh2DFromFunction(
                [](float t, float s) {
                    // Modified parametrization from Wikipedia
                    // https://en.wikipedia.org/wiki/Torus
                    return glm::vec3(
                        (0.7 + 0.2 * glm::cos(2 * M_PI * t)) * glm::cos(2 * M_PI * s),
                        // y direction is flipped to make the triangle orientation correct
                        0.25 * glm::sin(2 * M_PI * (1 - t)),
                        (0.7 + 0.2 * glm::cos(2 * M_PI * t)) * glm::sin(2 * M_PI * s)
                    );
                },
                [](float t, float s) {
                    return glm::vec3(
                        glm::cos(2 * M_PI * t) * glm::cos(2 * M_PI * s),
                        glm::sin(2 * M_PI * (1 - t)),
                        glm::cos(2 * M_PI * t) * glm::sin(2 * M_PI * s)
                    );
                },
                t.torus.fragments, true));
            break;
        default:
            throw std::runtime_error("Invalid object template");
        }

        objects.insert({t, ptr});
        return ptr;
    }
}
