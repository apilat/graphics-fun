#include "postprocessing.hpp"

#include <chrono>

namespace objects {
    void Post::setEnabled(PostEffects effect, bool enabled) {
        enabledMap[effect] = enabled;
    }

    bool Post::isEnabled(PostEffects effect) {
        auto it = enabledMap.find(effect);
        return it != enabledMap.end() && it->second;
    }

    void Post::bindShader() {
        glUseProgram(shader->id);

        shader->setUniformInt("flipx", isEnabled(FlipX));
        shader->setUniformInt("flipy", isEnabled(FlipY));
        shader->setUniformInt("invert", isEnabled(Invert));
        shader->setUniformInt("wave", isEnabled(Wave));
        shader->setUniformInt("scope", isEnabled(Scope));
        shader->setUniformInt("chromatic_aberration", isEnabled(ChromaticAberration));

        auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::system_clock::now().time_since_epoch()
        ).count();
        shader->setUniformFloat("time", (nanos % 1000000000) / 1e9);
    }

    void Post::unbindShader() {
        glUseProgram(0);
    }
}
