#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <string>
#include <cstring>
#include <optional>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "engine/mesh.hpp"
#include "engine/object.hpp"
#include "engine/render.hpp"
#include "engine/framebuffer.hpp"
#include "objects/objectset.hpp"
#include "objects/postprocessing.hpp"
#include "ui/ui.hpp"
#include "control.hpp"

static const int TPS = 60;
static const unsigned long long TICK_DELAY_NS = 1000000000 / TPS;

struct GameState {
    engine::Renderer r;
    engine::Object axes;
    objects::Set objectSet;
    objects::Template currentObject;

    engine::Framebuffer framebuffer;
    std::unique_ptr<engine::Mesh> fullscreenMesh;
    objects::Post postEffects;

    std::shared_ptr<control::EventQueue> evq;
    GLFWwindow* window;

    bool renderFull;
    bool renderWireframe;
    bool renderAxes;
    bool renderLight;
    bool renderTextures;

    int lastMouseState;
    glm::vec2 lastMousePosition;
};

static std::tuple<bool,glm::ivec2> windowSize;

GLFWwindow* createWindow();
GameState init(GLFWwindow*);
void render(GameState&);
void update(GameState&);
void input(GameState&);

int main() {
    windowSize = {true, {1024, 768}};
    GLFWwindow* window = createWindow();

    GameState state = init(window);

    struct timespec lastTime, nowTime;
    unsigned long long deltaNs = 0;
    unsigned long long frameCounter = 0, tickCounter = 0;

    clock_gettime(CLOCK_MONOTONIC, &nowTime);
    while (!glfwWindowShouldClose(window)) {
        lastTime = nowTime;
        clock_gettime(CLOCK_MONOTONIC, &nowTime);
        deltaNs += (nowTime.tv_sec - lastTime.tv_sec) * 1000000000
                 + (nowTime.tv_nsec - lastTime.tv_nsec);

        while (deltaNs > TICK_DELAY_NS) {
            update(state);
            deltaNs -= TICK_DELAY_NS;
            tickCounter++;
        }

        render(state);
        input(state);

        glfwSwapBuffers(window);
        glfwPollEvents();

        frameCounter++;
    }
}

void updateFramebufferSize(GLFWwindow* _window, int width, int height) {
    (void)_window;
    windowSize = {true, {width, height}};
}

GLFWwindow* createWindow() {
    if (!glfwInit()) {
        fputs("Failed to initialize GLFW", stderr);
        exit(1);
    }

    // OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    GLFWwindow* window;
    window = glfwCreateWindow(std::get<1>(windowSize).x, std::get<1>(windowSize).y, "Graphics Demonstration", NULL, NULL);
    if (!window) {
        fputs("Failed to open GLFW window on OpenGL 3.3", stderr);
        exit(1);
    }

    glfwSetFramebufferSizeCallback(window, updateFramebufferSize);
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) {
        fputs("Failed to initialize GLEW", stderr);
        exit(1);
    }

    return window;
}

GameState init(GLFWwindow* window) {
    std::shared_ptr<control::EventQueue> evq = std::make_shared<control::EventQueue>();
    control::startControlServer(evq).detach();

    auto fullscreenMesh = std::make_unique<engine::Mesh>((std::vector<GLfloat>){
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            1.0f, 1.0f, 0.0f
    });
    fullscreenMesh->getTextureCoords() = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 1.0f
    };
    fullscreenMesh->syncBuffers();

    auto axesMesh = std::make_unique<engine::Mesh>((std::vector<GLfloat>){
            -1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, 1.0f,
    });
    axesMesh->getColors() = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
    };
    axesMesh->syncBuffers();

    return GameState {
        engine::Renderer(),
        engine::Object(std::move(axesMesh), GL_LINES),
        objects::Set(),
        { objects::Cube },
        engine::Framebuffer(std::get<1>(windowSize).x, std::get<1>(windowSize).y, 4),
        std::move(fullscreenMesh),
        objects::Post(),
        evq,
        window,
        true, true, true, true, true,
        GLFW_RELEASE, glm::vec2(0),
    };
}

void render(GameState& state) {
    state.framebuffer.bindFramebuffer();
    state.r.clearBuffer();
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);
    glEnable(GL_DEPTH_TEST);

    if (state.renderFull) {
        state.r.bindShader("main");

        state.r.getCurShader()->setUniformInt("useTexture", state.renderTextures);
        state.r.getCurShader()->setUniformInt("light", state.renderLight);
        auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::system_clock::now().time_since_epoch()
        ).count();
        const float PI = 3.14159265;
        glm::vec3 lightSource(
                std::cos(nanos / 1e9 * 2.0 * PI / 10.0),
                0.5,
                std::sin(nanos / 1e9 * 2.0 * PI / 10)
        );
        state.r.getCurShader()->setUniformVec3("lightSource", lightSource);

        state.r.fill();
        state.objectSet.get(state.currentObject)->render(state.r);
        state.r.unbindShader();
    }

    if (state.renderWireframe) {
        state.r.bindShader("fixed_color");
        state.r.getCurShader()->setUniformVec3("color", glm::vec3(1.0f));
        state.r.wireframe();
        state.objectSet.get(state.currentObject)->render(state.r);
        state.r.unbindShader();
    }

    if (state.renderAxes) {
        state.r.bindShader("color");
        state.axes.render(state.r);
        state.r.unbindShader();
    }

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    state.framebuffer.unbindFramebuffer();

    state.r.clearBuffer();
    state.postEffects.bindShader();
    state.r.fill();
    state.framebuffer.bindTexture();
    state.fullscreenMesh->render();
    state.framebuffer.unbindTexture();
    state.postEffects.unbindShader();
}

void update(GameState& state) {
    if (std::get<0>(windowSize)) {
        GLsizei width = std::get<1>(windowSize).x;
        GLsizei height = std::get<1>(windowSize).y;
        glViewport(0, 0, width, height);
        state.framebuffer.resize(width, height);
        std::get<0>(windowSize) = false;
    }

    std::optional<std::string> data;

    // Change current object before its properties are updated
    if ((data = state.evq->get("shape"))) {
        if (data == "cube") {
            state.currentObject = { objects::Cube };
            state.renderTextures = true;
        } else if (data == "square") {
            state.currentObject = { objects::Square };
            state.renderTextures = true;
        } else if (data == "pyramid") {
            state.currentObject = { objects::Pyramid };
            state.renderTextures = true;
        } else if (data == "torus") {
            union objects::Template obj;
            obj.torus.ty = objects::Torus;
            obj.torus.fragments = 10;
            state.currentObject = obj;
            state.renderTextures = false;
        } else {
            std::cerr << "Unknown object " << *data << std::endl;
        }
    }
    if ((data = state.evq->get("torus_fragments")) && state.currentObject.ty == objects::Torus) {
        state.currentObject.torus.fragments = std::stof(*data);
    }

    if ((data = state.evq->get("rotation_x"))) {
        state.objectSet.get(state.currentObject)->rotation.x = std::stof(*data);
    }
    if ((data = state.evq->get("rotation_y"))) {
        state.objectSet.get(state.currentObject)->rotation.y = std::stof(*data);
    }
    if ((data = state.evq->get("rotation_z"))) {
        state.objectSet.get(state.currentObject)->rotation.z = std::stof(*data);
    }

    if ((data = state.evq->get("projection"))) {
        if (data == "orthographic") {
            state.r.projection.type = engine::projection::Orthographic;
            state.r.camera->rotation = glm::vec3(0);
        } else if (data == "perspective") {
            state.r.projection.type = engine::projection::Perspective;
            engine::projection::PerspectiveData projData = { 1 };
            memcpy(state.r.projection.data, &projData, sizeof(projData));
        } else if (data == "isometric") {
            state.r.projection.type = engine::projection::Orthographic;
            state.r.camera->rotation = glm::vec3(-0.61547971, 0.78539816, 0);
        } else {
            std::cerr << "Unknown projection " << *data << std::endl;
        }
    }
    if ((data = state.evq->get("perspective_dist")) && state.r.projection.type == engine::projection::Perspective) {
        engine::projection::PerspectiveData projData;
        projData.distance = std::stof(*data);
        memcpy(state.r.projection.data, &projData, sizeof(projData));
    }

    if ((data = state.evq->get("full"))) {
        state.renderFull = (*data)[0] == '1';
    }
    if ((data = state.evq->get("wireframe"))) {
        state.renderWireframe = (*data)[0] == '1';
    }
    if ((data = state.evq->get("axes"))) {
        state.renderAxes = (*data)[0] == '1';
    }
    if ((data = state.evq->get("light"))) {
        state.renderLight = (*data)[0] == '1';
    }

    if ((data = state.evq->get("flipx"))) {
        state.postEffects.setEnabled(objects::FlipX, (*data)[0] == '1');
    }
    if ((data = state.evq->get("flipy"))) {
        state.postEffects.setEnabled(objects::FlipY, (*data)[0] == '1');
    }
    if ((data = state.evq->get("invert"))) {
        state.postEffects.setEnabled(objects::Invert, (*data)[0] == '1');
    }
    if ((data = state.evq->get("wave"))) {
        state.postEffects.setEnabled(objects::Wave, (*data)[0] == '1');
    }
    if ((data = state.evq->get("scope"))) {
        state.postEffects.setEnabled(objects::Scope, (*data)[0] == '1');
    }
    if ((data = state.evq->get("chromatic_aberration"))) {
        state.postEffects.setEnabled(objects::ChromaticAberration, (*data)[0] == '1');
    }
}

void input(GameState& state) {
    glm::vec2 mousePos = ui::getMousePositionGlCoords(state.window);
    int mouseState = glfwGetMouseButton(state.window, GLFW_MOUSE_BUTTON_RIGHT);
    if (mouseState == GLFW_PRESS) {
        if (state.lastMouseState != GLFW_PRESS) {
            glfwSetInputMode(state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
        glm::vec2 posDiff = mousePos - state.lastMousePosition;
        state.r.camera->rotation += glm::vec3(-posDiff.y, posDiff.x, 0);
    } else if (state.lastMouseState == GLFW_PRESS && mouseState != GLFW_PRESS) {
        glfwSetInputMode(state.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    state.lastMousePosition = mousePos;
    state.lastMouseState = mouseState;
}
