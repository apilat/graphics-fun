#include "ui.hpp"

namespace ui {
    glm::vec2 getMousePositionGlCoords(GLFWwindow* window) {
        double mouseX, mouseY;
        int windowWidth, windowHeight;
        glfwGetCursorPos(window, &mouseX, &mouseY);
        glfwGetWindowSize(window, &windowWidth, &windowHeight);

        return {2 * mouseX / windowWidth - 1,
                -2 * mouseY / windowHeight + 1};
    }
}
