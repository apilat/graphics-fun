#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace ui {
    glm::vec2 getMousePositionGlCoords(GLFWwindow* window);
}
