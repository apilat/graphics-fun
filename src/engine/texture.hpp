#pragma once
#include <vector>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

namespace engine {
    class Texture {
        GLuint _id = 0;

        void genTexture();

        GLuint width, height;
        std::vector<uint8_t> data;

        Texture& operator=(const Texture&) = delete;
        Texture(const Texture&) = delete;
    public:
        // The API for texture is very similar to Mesh, which contains more explanations
        Texture(std::vector<uint8_t> data, GLuint width, GLuint height);
        Texture(std::string filename);
        ~Texture();

        std::vector<GLuint>& getData();

        void syncData();
        void flushData();
        void sendData();

        void bind();
        void unbind();
    };
}
