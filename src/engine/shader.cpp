#include "shader.hpp"

#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

namespace engine {
    Shader::Shader(std::string path, GLuint type) : id(glCreateShader(type)) {
        if (!this->id) {
            throw std::runtime_error("Shader creation failed");
        }

        // Reads whole file into the string `content`
        // https://stackoverflow.com/questions/2912520/read-file-contents-into-a-string-in-c
        std::ifstream ifs{path};
        std::string content{std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>()};

        char const* contentC = content.c_str();
        glShaderSource(id, 1, &contentC, nullptr);
        glCompileShader(id);

        GLint result, logLength;

        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLength);

        if (logLength > 0) {
            char* logMessage = new char[logLength + 1];
            glGetShaderInfoLog(id, logLength, NULL, logMessage);
            std::cerr << "Shader log (" << path << "): " << logMessage << std::endl;
            delete[] logMessage;
        }

        if (result != GL_TRUE) {
            glDeleteShader(id);
            throw std::runtime_error("Shader compilation failed");
        }
    }

    Shader::~Shader() {
        glDeleteShader(id);
    }

    Program::Program(std::unique_ptr<Shader> vertex, std::unique_ptr<Shader> fragment) : vertex(std::move(vertex)), fragment(std::move(fragment)), id(glCreateProgram()) {
        if (!this->id) {
            throw std::runtime_error("Program creation failed");
        }

        glAttachShader(this->id, this->vertex->id);
        glAttachShader(this->id, this->fragment->id);
        glLinkProgram(this->id);

        GLint result, logLength;
        glGetProgramiv(id, GL_LINK_STATUS, &result);
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logLength);

        if (logLength > 0) {
            char* logMessage = new char[logLength]();
            glGetProgramInfoLog(this->id, logLength, NULL, logMessage);
            std::cerr << "Program log: " << logMessage << std::endl;
            delete[] logMessage;
        }

        if (result != GL_TRUE) {
            glDeleteProgram(this->id);
            throw std::runtime_error("Program linking failed");
        }

        glDetachShader(this->id, this->vertex->id);
        glDetachShader(this->id, this->fragment->id);
    }

    Program::Program(std::string commonPath) :
        Program(std::make_unique<Shader>(commonPath + ".vert", GL_VERTEX_SHADER), std::make_unique<Shader>(commonPath + ".frag", GL_FRAGMENT_SHADER)) {}

    Program::~Program() {
        glDeleteProgram(this->id);
    }

    bool Program::hasUniformLocation(const std::string& name) {
        GLint uniformId = glGetUniformLocation(this->id, name.c_str());
        return uniformId != -1;
    }

    GLint Program::getUniformLocation(const std::string& name) {
        // TODO add caching
        GLint uniformId = glGetUniformLocation(this->id, name.c_str());
        if (uniformId == -1) {
            throw std::runtime_error("Invalid GLSL uniform: " + name);
        }
        return uniformId;
    }

    void Program::setUniformMat4(const std::string& name, glm::mat4 const& matrix) {
        glUniformMatrix4fv(this->getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Program::setUniformVec3(const std::string& name, glm::vec3 const& vector) {
        glUniform3fv(this->getUniformLocation(name), 1, glm::value_ptr(vector));
    }

    void Program::setUniformFloat(const std::string& name, GLfloat value) {
        glUniform1f(this->getUniformLocation(name), value);
    }

    void Program::setUniformInt(const std::string& name, GLint value) {
        glUniform1i(this->getUniformLocation(name), value);
    }
}
