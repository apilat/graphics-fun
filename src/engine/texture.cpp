#include "texture.hpp"

#include <stdexcept>
#include <cstdio>

namespace engine {
    void Texture::genTexture() {
        glGenTextures(1, &this->_id);
        if (!this->_id) {
            throw std::runtime_error("Texture creation failed");
        }
    }

    Texture::Texture(std::vector<uint8_t> data, GLuint width, GLuint height)
        : width(width), height(height), data(std::move(data)) {
        genTexture();
        syncData();
    }

    Texture::Texture(std::string filename) {
        // TODO support some sensible image formats
        FILE *fp = fopen(filename.c_str(), "rb");
        if (fp == NULL) {
            throw std::runtime_error("Cannot open texture file " + filename);
        }

        uint32_t size[2];
        if (fread(size, sizeof(uint32_t), 2, fp) != 2) {
            fclose(fp);
            throw std::runtime_error("Texture file " + filename + " has invalid format");
        }
        width = size[0];
        height = size[1];

        uint8_t *buf = (uint8_t*) malloc(65536);
        do {
            size_t read = fread(buf, 1, 65536, fp);
            data.insert(data.end(), buf, buf + read);
        } while (!feof(fp));

        free(buf);
        fclose(fp);

        genTexture();
        syncData();
    }

    Texture::~Texture() {
        glDeleteTextures(1, &this->_id);
    }

    void Texture::syncData() {
        glBindTexture(GL_TEXTURE_2D, _id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data.data());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void Texture::flushData() {
        this->data.clear();
    }

    void Texture::sendData() {
        syncData();
        flushData();
    }

    void Texture::bind() {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _id);
    }
    void Texture::unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
