#include "framebuffer.hpp"

#include <stdexcept>
#include <string>

namespace engine {
    Framebuffer::Framebuffer(GLuint width, GLuint height, GLsizei samples) : samples(samples), width(width), height(height) {
        glGenTextures(1, &textureIdFlat);
        glBindTexture(GL_TEXTURE_2D, textureIdFlat);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glBindTexture(GL_TEXTURE_2D, 0);

        glGenFramebuffers(1, &fboId);
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureIdFlat, 0);
        if (samples == 0) {
            // We only need a single rendebuffer bound to the FBO that's actually being rendered to.
            glGenRenderbuffers(1, &rboId);
            glBindRenderbuffer(GL_RENDERBUFFER, rboId);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);

            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboId);
        }

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            throw std::runtime_error("Framebuffer creation failed with status " + std::to_string(status));
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        if (samples > 0) {
            glGenTextures(1, &textureIdMultisample);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, textureIdMultisample);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGB, width, height, GL_TRUE);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

            glGenRenderbuffers(1, &rboId);
            glBindRenderbuffer(GL_RENDERBUFFER, rboId);
            glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT16, width, height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);

            glGenFramebuffers(1, &fboIdMultisample);
            glBindFramebuffer(GL_FRAMEBUFFER, fboIdMultisample);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textureIdMultisample, 0);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboId);

            GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if (status != GL_FRAMEBUFFER_COMPLETE) {
                throw std::runtime_error("Multisampled creation failed with status " + std::to_string(status));
            }
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    }

    Framebuffer::~Framebuffer() {
        glDeleteTextures(1, &textureIdFlat);
        glDeleteRenderbuffers(1, &rboId);
        glDeleteFramebuffers(1, &fboId);
        if (samples > 0) {
            glDeleteTextures(1, &textureIdMultisample);
            glDeleteFramebuffers(1, &fboIdMultisample);
        }
    }

    void Framebuffer::resize(GLuint width, GLuint height) {
        this->width = width;
        this->height = height;

        glBindTexture(GL_TEXTURE_2D, textureIdFlat);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glBindTexture(GL_TEXTURE_2D, 0);

        if (samples == 0) {
            glBindRenderbuffer(GL_RENDERBUFFER, rboId);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
        }

        if (samples > 0) {
            glBindRenderbuffer(GL_RENDERBUFFER, rboId);
            glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT16, width, height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);

            // Without recreating the texture, this creates a memory leak. I'm not sure if this is an unintended
            // memory leak in nouveau or a misuse of the API. There is little info online.
            glDeleteTextures(1, &textureIdMultisample);
            glGenTextures(1, &textureIdMultisample);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, textureIdMultisample);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGB, width, height, GL_TRUE);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

            glBindFramebuffer(GL_FRAMEBUFFER, fboIdMultisample);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textureIdMultisample, 0);
            GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if (status != GL_FRAMEBUFFER_COMPLETE) {
                throw std::runtime_error("Multisampled texture re-creation failed with status " + std::to_string(status));
            }
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    }

    void Framebuffer::bindFramebuffer() {
        if (samples > 0) {
            glBindFramebuffer(GL_FRAMEBUFFER, fboIdMultisample);
        } else {
            glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        }
    }
    void Framebuffer::unbindFramebuffer() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void Framebuffer::bindTexture() {
        if (samples > 0) {
            glBindFramebuffer(GL_READ_FRAMEBUFFER, fboIdMultisample);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fboId);
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
        glBindTexture(GL_TEXTURE_2D, textureIdFlat);
    }
    void Framebuffer::unbindTexture() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
