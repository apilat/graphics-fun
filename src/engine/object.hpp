#pragma once
#include <memory>
#include <GL/glew.h>
#include <glm/glm.hpp>

namespace engine {
    class Mesh;
    class Renderer;
    class Texture;

    class Object {
    public:
        const std::unique_ptr<Mesh> mesh;
        const std::unique_ptr<Texture> texture;
        glm::vec3 position;
        glm::vec3 rotation; // Euler angles
        glm::vec3 scale;
        const GLenum mode = GL_TRIANGLES;

        Object();
        Object(std::string name);
        Object(std::unique_ptr<Mesh> mesh);
        Object(std::unique_ptr<Mesh> mesh, GLenum mode);

        glm::mat4 getModelMatrix() const;

        void render(Renderer& r);
    };
}
