#pragma once
#include <map>
#include <memory>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

namespace engine {
    class Program;
    class Object;
    class Texture;

    namespace projection {
        enum ProjectionType {
            Orthographic,
            Perspective,
        };

        struct Projection {
            ProjectionType type;
            char data[24];
        };

        struct PerspectiveData {
            float distance;
        };
        static_assert(sizeof(PerspectiveData) <= 24, "PerspectiveData does not fit inside Projection");
    }

    class Renderer {
        std::map<std::string, std::shared_ptr<Program>> shaders;
        std::shared_ptr<Program> currentShader = nullptr;

    public:
        projection::Projection projection = { projection::Orthographic, { 0 }};
        std::unique_ptr<Object> camera;
        Renderer();

        void clearBuffer();

        void addShader(std::string name);
        void addShader(std::string name, std::shared_ptr<Program> shader);
        std::shared_ptr<Program> getCurShader();
        void bindShader(std::string name);
        void unbindShader();

        glm::mat4 getProjectionMatrix();

        void fill();
        void wireframe();
    };
}
