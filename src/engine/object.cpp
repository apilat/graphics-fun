#include "object.hpp"

#include "mesh.hpp"
#include "render.hpp"
#include "shader.hpp"
#include "texture.hpp"

namespace engine {
    Object::Object()
        : mesh(nullptr), texture(nullptr),
        position(glm::vec3(0.0f)), rotation(glm::vec3(0.0f)), scale(glm::vec3(1.0f)) { }

    Object::Object(std::string name)
        : mesh(std::make_unique<Mesh>(name + ".obj")), texture(std::make_unique<Texture>(name + ".dat")),
        position(glm::vec3(0.0f)), rotation(glm::vec3(0.0f)), scale(glm::vec3(1.0f)) { }

    Object::Object(std::unique_ptr<Mesh> mesh)
        : mesh(std::move(mesh)), texture(nullptr), position(glm::vec3(0.0f)), rotation(glm::vec3(0.0f)), scale(glm::vec3(1.0f)) { }
    Object::Object(std::unique_ptr<Mesh> mesh, GLenum mode)
        : mesh(std::move(mesh)), texture(nullptr), position(glm::vec3(0.0f)), rotation(glm::vec3(0.0f)), scale(glm::vec3(1.0f)), mode(mode) { }

    glm::mat4 Object::getModelMatrix() const {
        auto scale_matrix = glm::mat4(
                this->scale.x, 0.0f, 0.0f, 0.0f,
                0.0f, this->scale.y, 0.0f, 0.0f,
                0.0f, 0.0f, this->scale.z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        );
        auto position_matrix = glm::mat4(
                1.0f, 0.0f, 0.0f, this->position.x,
                0.0f, 1.0f, 0.0f, this->position.y,
                0.0f, 0.0f, 1.0f, this->position.z,
                0.0f, 0.0f, 0.0f, 1.0f
        );
        auto rotation_matrix = glm::mat4(
            glm::mat3(
                glm::cos(this->rotation.z), -glm::sin(this->rotation.z), 0.0f,
                glm::sin(this->rotation.z), glm::cos(this->rotation.z), 0.0f,
                0.0f, 0.0f, 1.0f
            ) * glm::mat3(
                glm::cos(this->rotation.y), 0.0f, glm::sin(this->rotation.y),
                0.0f, 1.0f, 0.0f,
                -glm::sin(this->rotation.y), 0.0f, glm::cos(this->rotation.y)
            ) * glm::mat3(
                1.0f, 0.0f, 0.0f,
                0.0f, glm::cos(this->rotation.x), -glm::sin(this->rotation.x),
                0.0f, glm::sin(this->rotation.x), glm::cos(this->rotation.x)
            )
        );
        return position_matrix * rotation_matrix * scale_matrix;
    }

    void Object::render(Renderer& r) {
        if (texture) {
            texture->bind();
        }

        // We use the transpose of the actual matrix since OpenGL uses column-major ordering.
        // https://stackoverflow.com/questions/13293469/why-does-my-translation-matrix-needs-to-be-transposed
        auto modelMatrix = getModelMatrix();
        r.getCurShader()->setUniformMat4("trans", glm::transpose(
            // Because the final matrix needs to be transposed, we also need to reverse the multiplication order.
            modelMatrix * r.camera->getModelMatrix() * r.getProjectionMatrix()
        ));
        if (r.getCurShader()->hasUniformLocation("model")) {
            r.getCurShader()->setUniformMat4("model", glm::transpose(modelMatrix));
        }

        mesh->render(mode);
        if (texture) {
            texture->unbind();
        }
    }
}
