#pragma once
#include <string>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

namespace engine {
    class Shader {
        Shader& operator=(const Shader&) = delete;
        Shader(const Shader&) = delete;
    public:
        const GLuint id;
        Shader(std::string path, GLuint type);
        ~Shader();
    };

    class Program {
        const std::unique_ptr<Shader> vertex, fragment;
        GLint getUniformLocation(const std::string& name);

        Program& operator=(const Program&) = delete;
        Program(const Program&) = delete;
    public:
        const GLuint id;

        Program(std::unique_ptr<Shader> vertex, std::unique_ptr<Shader> fragment);
        Program(std::string commonPath);
        ~Program();

        bool hasUniformLocation(const std::string& name);
        void setUniformMat4(const std::string& name, glm::mat4 const& matrix);
        void setUniformVec3(const std::string& name, glm::vec3 const& vector);
        void setUniformFloat(const std::string& name, GLfloat value);
        void setUniformInt(const std::string& name, GLint value);
    };
}
