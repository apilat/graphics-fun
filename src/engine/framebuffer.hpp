#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

namespace engine {
    class Framebuffer {
        GLuint fboId, fboIdMultisample;
        GLuint textureIdFlat, textureIdMultisample;
        GLuint rboId;
        const GLsizei samples;
        GLuint width, height;

        Framebuffer(const Framebuffer&) = delete;
        Framebuffer& operator=(const Framebuffer&) = delete;
    public:
        Framebuffer(GLuint width, GLuint height, GLsizei samples);
        ~Framebuffer();

        void resize(GLuint width, GLuint height);

        void bindFramebuffer();
        void unbindFramebuffer();
        void bindTexture();
        void unbindTexture();
    };
}
