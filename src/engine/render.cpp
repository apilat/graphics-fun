#include "render.hpp"

#include "shader.hpp"
#include "object.hpp"
#include "mesh.hpp"
#include "texture.hpp"

#include <stdexcept>

namespace engine {
    Renderer::Renderer() : camera(std::make_unique<Object>()) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    void Renderer::addShader(std::string name) {
        this->addShader(name, std::make_shared<engine::Program>("resources/" + name));
    }
    void Renderer::addShader(std::string name, std::shared_ptr<Program> shader) {
        this->shaders.insert(std::pair<std::string, std::shared_ptr<Program>>(name, std::move(shader)));
    }

    void Renderer::clearBuffer() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void Renderer::bindShader(std::string name) {
        auto shader = this->shaders.find(name);
        if (shader == this->shaders.end()) {
            this->addShader(name);
            shader = this->shaders.find(name);
            if (shader == this->shaders.end()) {
                throw std::runtime_error("Unknown shader: " + name);
            }
        }
        this->currentShader = shader->second;
        glUseProgram(shader->second->id);
    }

    std::shared_ptr<Program> Renderer::getCurShader() {
        return this->currentShader;
    }

    void Renderer::unbindShader() {
        glUseProgram(0);
    }

    void Renderer::fill() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    void Renderer::wireframe() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    glm::mat4 Renderer::getProjectionMatrix() {
        switch (this->projection.type) {
            case projection::Orthographic:
                return glm::mat4(
                        1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                );
            case projection::Perspective:
                {
                    float cameraDistance = ((projection::PerspectiveData*) &this->projection.data)->distance;
                    return glm::mat4(
                            cameraDistance, 0.0f, 0.0f, 0.0f,
                            0.0f, cameraDistance, 0.0f, 0.0f,
                            0.0f, 0.0f, 1.0f, 0.0f,
                            0.0f, 0.0f, 1.0f, cameraDistance + 1.0f
                    );
                }
            default:
                throw std::runtime_error("Unknown projection");
        }
    }
}
