#include "mesh.hpp"

#include <fstream>
#include "render.hpp"
#include "object.hpp"

namespace engine {
    Mesh::Mesh(std::vector<GLfloat> positions) {
        this->genVertexArray();
        this->positions = positions;
        syncBuffers();
    }

    Mesh::Mesh(std::vector<GLfloat> positions, std::vector<GLuint> indices) {
        this->genVertexArray();
        this->positions = positions;
        this->indices = indices;
        syncBuffers();
    }

    void Mesh::genVertexArray() {
        glGenVertexArrays(1, &this->_vaoId);
        if (!this->_vaoId) {
            throw std::runtime_error("VAO creation failed");
        }
    }

    void Mesh::bufferFloats(std::vector<GLfloat> const& data, GLuint position, GLuint unitSize) {
        if (!this->_vboIds[position]) {
            glGenBuffers(1, &this->_vboIds[position]);
            if (!this->_vboIds[position]) {
                throw std::runtime_error("VBO creation failed");
            }
        }
        glBindVertexArray(this->_vaoId);
        glEnableVertexAttribArray(position);
        glBindBuffer(GL_ARRAY_BUFFER, this->_vboIds[position]);

        glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(data[0]), data.data(), GL_STATIC_DRAW);
        glVertexAttribPointer(position, unitSize, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(position);
        glBindVertexArray(0);
    }

    void Mesh::bufferIndices(std::vector<GLuint> const& data, GLuint position) {
        if (!this->_vboIds[position]) {
            glGenBuffers(1, &this->_vboIds[position]);
            if (!this->_vboIds[position]) {
                throw std::runtime_error("VBO creation failed");
            }
        }
        glBindVertexArray(this->_vaoId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_vboIds[position]);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * sizeof(data[0]), data.data(), GL_STATIC_DRAW);

        glBindVertexArray(0);
    }

    Mesh::~Mesh() {
        glDeleteVertexArrays(1, &this->_vaoId);
        for (unsigned i = 0; i < VAO_SIZE; i++) {
            if (this->_vboIds[i])
                glDeleteBuffers(1, &this->_vboIds[i]);
        }
        // FIXME not sure if destructors for buffers need to be explicitly called or are automatically cleaned up
    }

    Mesh::Mesh(std::string path) {
        std::ifstream file(path);
        if (!file) {
            throw std::runtime_error("Could not open mesh: " + path);
        }

        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> texCoords;
        std::vector<glm::vec3> normals;
        std::map<std::tuple<int,int,int>, size_t> existingVertices;
        std::string line;
        float x, y, z;
        int v[3], t[3], n[3];
        while (std::getline(file, line)) {
            if (sscanf(line.c_str(), "v %f %f %f", &x, &y, &z) == 3) {
                vertices.push_back({x, y, z});
            } else if (sscanf(line.c_str(), "vt %f %f", &x, &y) == 2) {
                texCoords.push_back({x, y});
            } else if (sscanf(line.c_str(), "vn %f %f %f", &x, &y, &z) == 3) {
                normals.push_back({x, y, z});

            } else if (sscanf(line.c_str(), "f %d/%d/%d %d/%d/%d %d/%d/%d", &v[0], &t[0], &n[0], &v[1], &t[1], &n[1], &v[2], &t[2], &n[2]) == 9) {
                for (int i = 0; i < 3; i++) {
                    --v[i], --t[i], --n[i]; // Convert 1-based indexing to 0-based
                    std::tuple<int,int,int> vertexData = {v[i], t[i], n[i]};
                    auto it = existingVertices.find(vertexData);
                    if (it != existingVertices.end()) {
                        // Reuse existing vertex data
                        this->indices.push_back(it->second);
                    } else {
                        // Create new OpenGL vertex
                        this->positions.push_back(vertices[v[i]].x);
                        this->positions.push_back(vertices[v[i]].y);
                        this->positions.push_back(vertices[v[i]].z);
                        this->textureCoords.push_back(texCoords[t[i]].x);
                        this->textureCoords.push_back(1 - texCoords[t[i]].y);
                        this->normals.push_back(normals[n[i]].x);
                        this->normals.push_back(normals[n[i]].y);
                        this->normals.push_back(normals[n[i]].z);
                        size_t index = this->positions.size() / 3 - 1;
                        existingVertices.insert({vertexData, index});
                        this->indices.push_back(index);
                    }
                }
            }
        }

        this->genVertexArray();
        this->syncBuffers();
    }

    GLuint Mesh::vaoId() const {
        return this->_vaoId;
    }

    GLuint Mesh::vertexCount() const {
        if (this->usesIndices())
            return this->indices.size();
        else
            return this->positions.size() / 3;
    }

    bool Mesh::usesIndices() const {
        return this->indices.size() > 0;
    }

    std::vector<GLfloat>& Mesh::getPositions() {
        return this->positions;
    }
    std::vector<GLfloat>& Mesh::getColors() {
        return this->colors;
    }
    std::vector<GLuint>& Mesh::getIndices() {
        return this->indices;
    }
    std::vector<GLfloat>& Mesh::getTextureCoords() {
        return this->textureCoords;
    }
    std::vector<GLfloat>& Mesh::getNormals() {
        return this->normals;
    }

    void Mesh::syncBuffers() {
        if (positions.size() > 0)
            this->bufferFloats(this->positions, POSITIONS_VBO, 3);
        if (indices.size() > 0)
            this->bufferIndices(this->indices, INDICES_VBO);
        if (colors.size() > 0)
            this->bufferFloats(this->colors, COLORS_VBO, 3);
        if (textureCoords.size() > 0)
            this->bufferFloats(this->textureCoords, TEXTURE_VBO, 2);
        if (normals.size() > 0)
            this->bufferFloats(this->normals, NORMALS_VBO, 3);
    }

    void Mesh::flushBuffers() {
        this->positions.clear();
        this->indices.clear();
        this->colors.clear();
        this->textureCoords.clear();
        this->normals.clear();
    }

    void Mesh::sendBuffers() {
        this->syncBuffers();
        this->flushBuffers();
    }

    void Mesh::render(GLenum mode) {
        glBindVertexArray(this->vaoId());

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(4);

        if (this->usesIndices())
            glDrawElements(mode, this->vertexCount(), GL_UNSIGNED_INT, 0);
        else
            glDrawArrays(mode, 0, this->vertexCount());

        glDisableVertexAttribArray(4);
        glDisableVertexAttribArray(3);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);

        glBindVertexArray(0);
    }
}
