#pragma once
#include <vector>
#include <string>
#include <optional>

#include <GL/glew.h>
#include <GL/gl.h>

namespace engine {
    class Mesh {
        static const unsigned VAO_SIZE = 5;
        static const GLuint POSITIONS_VBO = 0;
        static const GLuint COLORS_VBO = 1;
        static const GLuint INDICES_VBO = 2;
        static const GLuint TEXTURE_VBO = 3;
        static const GLuint NORMALS_VBO = 4;
        GLuint _vaoId = 0;
        GLuint _vboIds[VAO_SIZE] = {0};

        void genVertexArray();
        void bufferFloats(std::vector<GLfloat> const& data, GLuint position, GLuint unitSize);
        void bufferIndices(std::vector<GLuint> const& data, GLuint position);

        std::vector<GLfloat> positions, colors, textureCoords, normals;
        std::vector<GLuint> indices;

        Mesh(const Mesh&) = delete;
        Mesh& operator=(const Mesh&) = delete;
    public:
        // By default all constructors sync data to GPU but maintain buffers on CPU.
        // Call flushBuffers() to clear that memory.
        Mesh(std::vector<GLfloat> positions);
        Mesh(std::vector<GLfloat> positions, std::vector<GLuint> indices);
        // There are no constructors for colors or textureCoords since they would collide.
        // Instead populate the buffer with getColors() or getTextureCoords() and then call syncBuffers().
        Mesh(std::string path);
        ~Mesh();

        GLuint vaoId() const;
        GLuint vertexCount() const;
        bool usesIndices() const;

        // NB: syncBuffers or sendBuffers needs to be called after modifying any buffers to flush to GPU
        std::vector<GLfloat>& getPositions();
        std::vector<GLfloat>& getColors();
        std::vector<GLuint>& getIndices();
        std::vector<GLfloat>& getTextureCoords();
        std::vector<GLfloat>& getNormals();

        // Sync buffers to GPU
        void syncBuffers();
        // Flush buffers on CPU
        // For now flush should not be used since it disrupts the functioning of vertexCount() and usesIndices()
        void flushBuffers();
        // Sync + Flush
        void sendBuffers();

        void render(GLenum mode = GL_TRIANGLES);
    };
}
